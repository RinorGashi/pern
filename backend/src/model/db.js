const {Pool} = require("pg");

const pool = new Pool({
    host: "db",
    port: 5432,
    user: 'postgres',
    password: 'test'
});

/**
 * This function creates the table in our database.
 * @param isTest - is True if function is being tested
 * @returns {Promise<void>}
 */
async function createTable(isTest) {
    const table = isTest ? "testTable" : "people";
    const initialQuery = "CREATE TABLE " + table + "(" +
        "firstName varchar(15)," +
        "lastName varchar(15)," +
        "gender varchar(25)," +
        "password varchar(20)" +
        ");";
    /*
    await pool.query(initialQuery, (err) => {
        if (err) {
            switch (err) {
                case err.errno === 'ENOTFOUND':
                    throw new Error('CANT CONNECT TO DB!');
                case err.code === '43P07':
                    throw new Error('TABLE ALREADY EXISTS');
                default:
                    console.log(err);
                    break;
            }
        } else {
            console.log("CREATED NEW TABLE!")
        }
        console.log("READY FOR REQUESTS!");
    });
     */
    await pool.query(initialQuery).catch((err) => {
        if (err.errno == 'ENOTFOUND') {
            throw new Error('CANT CONNECT TO DB!');
        } else if (err.code == '43P07') {
            throw new Error('TABLE ALREADY EXISTS!');
        } else {
            console.error(err);
        }
    })
}

/**
 * This function creates a new user in our database.
 * @param firstName
 * @param lastName
 * @param gender
 * @param password
 * @param isTest - is True if function is being tested
 * @returns {Promise<void>}
 */
async function create(firstName, lastName, gender, password, isTest) {
    const table = isTest ? "testTable" : "people"
    const text = "INSERT INTO " + table + " VALUES($1, $2, $3, $4)";
    const values = [firstName, lastName, gender, password];
    await pool.query(text, values, (err, result) => {
        if (err) {
            console.error(err);
        }
    });
}

/**
 * This function returns data from any demanded user.
 * @param user
 * @param isTest - is True if function is being tested
 * @returns {Promise<query|void|PermissionStatus>}
 */
async function read(user, isTest) {
    const table = isTest ? "testTable" : "people"
    const text = "SELECT * FROM " + table + " WHERE lastname LIKE ($1)";
    const name = ['%' + user + '%'];
    return readHandler(text, name);
}

/**
 * This functions returns data of all users. Invoked when no name is entered.
 * @param isTest - is True if function is being tested
 * @returns {Promise<query|void|PermissionStatus>}
 */
async function readAll(isTest) {
    const table = isTest ? "testTable" : "people"
    const text = "SELECT * FROM " + table;
    return readHandler(text, '');
}

/**
 * This function updates a users data.
 * @param req
 * @param isTest - is True if function is being tested
 * @returns {Promise<void>}
 */
async function update(req, isTest) {
    const table = isTest ? "testTable" : "people"
    const text = "UPDATE " + table + " SET firstName = ($1), gender=($3), password=($4) WHERE lastName= ($2);";
    const values = [req.body.firstName, req.body.lastName, req.body.gender, req.body.password];
    await pool.query(text, values, (err, result) => {
        if (err) {
            console.error(err);
        }
    });
}

/**
 * This function deletes a user.
 * @param req
 * @param isTest - is True if function is being tested
 * @returns {Promise<void>}
 */
async function del(req, isTest) {
    const table = isTest ? "testTable" : "people"
    const text = "DELETE FROM " + table + " WHERE firstName=($1) AND lastName=($2);";
    const name = [req.body.firstName, req.body.lastName];
    await pool.query(text, name, (err, result) => {
        if (err) {
            console.error('LOL', err);
        }
    });
}

async function readHandler(text, name) {
    var resultOfDB;
    await pool.query(text, name).then((result) => {
        resultOfDB = result.rows;
    }).catch((err) => {
        console.error(err);
    })
    return resultOfDB;
}

module.exports = {createTable, create, read, readAll, update, del};