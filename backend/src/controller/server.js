// CONSTANTS
const express = require('express');
const cors = require('cors');
const {create, read, update, del, createTable, readAll} = require("../model/db");
const app = express();

/**
 * This function is used for responding to READ requests.
 * @param databaseResponse
 */
const responseOfReadRequest = (res, databaseResponse) => {
    if (databaseResponse) {
        res.status(200).send(databaseResponse);
    } else {
        res.status(500).send();
    }
}

/**
 * This function is used for responding to PUT and DELETE requests.
 * @param kindOfRequest
 * @param res
 */
const responseOfPutAndDeleteRequest = (kindOfRequest, res) => {
    try {
        kindOfRequest();
        res.status(204).send();
    } catch (error) {
        res.status(500).send();
    }
}

app.use(cors());
app.use(express.json());

// CREATE TABLE
createTable();

// CRUD STUFF //
// CREATE
// TODO remove try catch
app.post("/", (req, res) => {
    try {
        create(req.body.firstName, req.body.lastName, req.body.gender, req.body.password);
        res.status(201).send();
    } catch (error) {
        res.status(400).send();
    }
});

// READ
app.get("/:user", async (req, res) => {
    responseOfReadRequest(res, await read(req.params.user));
});

app.get('/', async (req, res) => {
    responseOfReadRequest(res, await readAll());
});

// UPDATE
app.put("/", (req, res) => {
    const qry = "UPDATE people SET password = '" + req.body.password + "' WHERE name='" + req.body.name + "';";
    responseOfPutAndDeleteRequest(update(req), res);
});

// DELETE
app.delete("/", (req, res) => {
    responseOfPutAndDeleteRequest(del(req), res);
});

// SOME OTHER STUFF
app.listen(8000, () => {
    console.log("API IS RUNNING!")
});