// imports
import React from "react";
import Button from "./Button";
import "./FindUser.scss";
import search from "./common/Search";

// variables
var userChangable = false;
var userDeletable = false;


/**
    This is the FindUser sub-page
    @Param props - properties
    @Return JSX-Element
 */
export default function FindUser(props) {
    userChangable = props.changableUsers;
    userDeletable = props.deletableUsers;
    return (
        <div>
            <div id="main">
                <input id="searchBar"/>
                <Button special="search" text="SEARCH" function={search}/>
                <br/>
            </div>
            <div id="people">
            </div>
        </div>
    )
}
