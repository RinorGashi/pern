/**
 * This is Rinor's super Cool User-Tool react application.
 * It provides you with state of the art technology when it comes to handling user-data in the most unsafe way possible.
 * That's why we don't use any kind of encription or anything.
 * Have fun using our premium freeware tool. Feel free to give me all your money even though it's free.
 * I will buy a lot of cool stuff and maybe (or maybe not) update this software someday.
 * Good feedback on github would be nice, even though i will fake all good ones by myself.
 * Thank you anyway.
 *
 * @Version 0.0.0.4.5.2.1 Beta
 *
 * @Copyright RinorGashi 2020 - All rights reserved
 *
 * @Contact rinor.gsi@gmail.com | www.rinoristderbeste.de
 */

// imports
import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import Header from './Header';
import FindUser from './FindUser';
import AddUser from "./AddUser";
import InitialLoad from "./InitialLoad";

/**
    This function renders the given Component in the 'inhalt' div.
    @Param component given by other function
    @Return no return
 */
const renderChosenSubpage = (component) => {
    ReactDOM.render(
        <React.StrictMode>
            {component}
        </React.StrictMode>,
        document.getElementById("inhalt")
    );
}

/*
    This function-call renders the header.
 */
ReactDOM.render(
    <React.StrictMode>
        <Header button_searchUser={() => renderChosenSubpage(
            <FindUser changableUsers={false} deletableUsers={false}/>
        )} button_addUser={() => renderChosenSubpage(
            <AddUser formType='add'/>
        )}/>
    </React.StrictMode>,
    document.getElementById('root')
);

/*
    This function-call renders the 'standard-page' in the beginning
 */
ReactDOM.render(
    <React.StrictMode>
        <InitialLoad/>
    </React.StrictMode>,
    document.getElementById('inhalt')
);

