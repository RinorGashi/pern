// imports
import addUser from "./AddUserFunction";
import renderError from "./RenderError";

/**
 This function is invoked after the user clicks on 'SUBMIT'. It is used for checking if input is okay.
 Not entirely finished yet, but works somehow.
 @Param no params
 @Return no return
 */
export default function submit (currGender, fn, ln){
    var pw = document.getElementById('pwOne').value;
    var pwStatus = 'unknown';
    var isChange = true;
    if(!fn & !ln){
        fn = document.getElementById('fNI').value;
        ln = document.getElementById('lNI').value;
        isChange = false;
    }

    // CHECK IF PASSWORD IS OKAY
    if (pw == document.getElementById('pwTwo').value) {
        renderError(true);
        pwStatus = 'ok';
    } else {
        renderError(false);
        pwStatus = 'nok';
    }
    if (fn == true || ln == true || currGender != 'None' || pwStatus == 'ok') {
        addUser({
            firstName: fn.toLowerCase(),
            lastName: ln.toLowerCase(),
            gender: currGender.toLowerCase(),
            password: pw
        }, isChange);
    }
}


