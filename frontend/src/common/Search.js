// imports
import axios from "axios";
import displayAllResults from "./DisplayAllResults";

/**
 This function sends a get request to our server.
 @Info this method will be removed and added to a seperate file - because of redundancy
 @Param no params
 @Return no return
 */
export default function search(){
    var inputValue = document.getElementById('searchBar').value;
    console.log(inputValue);
    if (!inputValue) {
        inputValue = '';
    }
    axios.get('http://localhost:8000/' + inputValue)
        .then(res => {
            displayAllResults(res);
        });
}