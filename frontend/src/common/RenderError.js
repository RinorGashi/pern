// imports
import ReactDOM from "react-dom";
import React from "react";

/**
 This function is invoked when input is not okay. In this case only if password is not okay.
 @Info this method will be removed and added to a seperate file - because of redundancy
 @Param passwordOkay -> Boolean, whether password is okay or not
 @Return no return
 */
export default function renderError(passwordOkay){
    var elem;
    if (passwordOkay) {
        elem = <></>;
    } else {
        elem = <p id="error">PASSWORD MUST BE THE SAME IN BOTH!</p>;
    }
    ReactDOM.render(
        <React.StrictMode>
            <div>
                {elem}
            </div>
        </React.StrictMode>, document.getElementById('pwNotification')
    );
}