// imports
import axios from "axios";
import ReactDOM from "react-dom";
import React from "react";

/**
 This function sends a post-request to our REST-API.
 @Info this method will be removed and added to a seperate file - because of redundancy
 @Param Data -> Contains: firstName, lastName, gender, password
 @Return no return
 */
export default function addUser(data, isChange){
    var request = axios.post;
    if(isChange){
        request = axios.put;
    }
    request('http://localhost:8000', data).then((res) => {
        if (res.status != 200)
            throw new Error();
    }).catch((err) => {
        console.log(err)
    }).finally(() => {
        // SUCCESS
        ReactDOM.render(
            <React.StrictMode>
                <div>
                    <p>USER ADDED!</p>
                </div>
            </React.StrictMode>, document.getElementById('inhalt')
        );
    })
}