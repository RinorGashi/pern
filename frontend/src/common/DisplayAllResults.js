// imports
import User from "../User";
import ReactDOM from "react-dom";
import React from "react";

/**
 * This function renders all people returned by our API.
 * @Info this method will be removed and added to a seperate file - because of redundancy
 * @Param array of people
 * @Return no return
 */
export default function displayAllResults (objects) {
    var elems = [];
    for (var i = 0; i < objects.data.length; i++) {
        var fiNa = objects.data[i].firstname;
        var laNa = objects.data[i].lastname;
        var gen = objects.data[i].gender;
        var paw = objects.data[i].password;
        elems.push(<User fn={fiNa} ln={laNa} g={gen}
                         pw={paw}/>);
    }
    console.log(elems.length);
    ReactDOM.render(
        <React.StrictMode>
            {elems}
        </React.StrictMode>, document.getElementById('people')
    );
}