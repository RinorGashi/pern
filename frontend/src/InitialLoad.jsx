// imports
import React from "react";
import "./InitialLoad.css";

/**
 * This is something i havent done myself. Found it online
 * @Param props - properties
 * @Return JSX-Element
 */
export default function InitialLoad(props) {
    return (
      <div id="main">
          <div className="lds-grid">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
          </div>
      </div>
    );
}