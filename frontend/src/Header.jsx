// imports
import React from "react";
import "./Header.scss";
import Button from "./Button";

/**
 *  This is the Header
 * @Param props
 * @Return JSX-Element
 */
export default function Header (props) {
    return(
        <div>
            <h1>USER-TOOL</h1>
            <hr/>
            <span className="buttons">
                <Button special="none" text="find user" function={props.button_searchUser}/>
                <Button special="none" text="add user" function={props.button_addUser}/>
            </span>
        </div>
    )
}