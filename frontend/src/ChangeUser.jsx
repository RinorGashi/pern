// import
import React from 'react';
import Button from "./Button";
import submit from "./common/Submit";

// variables
var currGender = "None";
var fn;
var ln;
var gOld;
var pwOld;

/**
 This function updates the variable 'currGender' with the chose gender. It is invoked when clicked on a gender-button
 @Param The newly chosen gender
 @Return no return
 */
const choseGender = (gender) => {
    currGender = gender;
}

/**
 This is the ChangeUser-Subpage
 @Param props - properties
 @Return JSX-Element
 */
export default function (props) {
    fn = props.userData.firstName;
    ln = props.userData.lastName;
    gOld = props.userData.gender;
    pwOld = props.userData.password;
    return (
        <div>
            <div className="Parameter" id="firstName">
                <p>First Name</p>
                <p>{props.userData.firstName}</p>
            </div>
            <div className="Parameter" id="lastName">
                <p>Last Name</p>
                <p>{props.userData.lastName}</p>
            </div>
            <div className="Parameter" id="gender">
                <p>GENDER</p>
                <Button text="MALE" special="gender" function={() => choseGender("male")}></Button>
                <Button text="FEMALE" special="gender" function={() => choseGender("female")}></Button>
                <Button text="ATTACKHELICOPTER" special="gender"
                        function={() => choseGender("attackhelicopter")}></Button>
            </div>
            <div className="Parameter" id="password">
                <p>Password</p>
                <input type="password" id="pwOne"/>
            </div>
            <div className="Parameter" id="repPassword">
                <p>Repeat Password</p>
                <input type="password" id="pwTwo"/>
            </div>
            <div id="pwNotification">

            </div>
            <div className="Parameter">
                <Button text="SUBMIT CHANGES" special="submit" function={() => submit(currGender, fn, ln)}/>
            </div>
        </div>
    )
}
