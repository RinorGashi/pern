// imports
import React from "react";
import "./Button.scss";
import axios from 'axios';
import ChangeUser from "./ChangeUser";
import ReactDOM from "react-dom";
import search from "./common/Search";

/**
    This function re-renders the part where the current users are shown. It sends a put request to our API.
    @Param the new user-data that will be sent to our API
    @Return no return
 */
const editUser = (newUserData) => {
    ReactDOM.render(
        <React.StrictMode>
            <ChangeUser userData={newUserData}/>
        </React.StrictMode>, document.getElementById('people')
    );
    axios.put('http://localhost:8000/', newUserData);
}

/**
 * This function is invoked when clicked on delete. It sends a delete request to our API.
 * @Param user-data of the person that needs to be deleted
 * @Return no return
 */
const deleteUser = (userDataToDelete) => {
    axios.delete('http://localhost:8000/', { data : userDataToDelete});
    search();
}

/**
    This is a Button.
    @Param props - properties
    @Return JSX-Element
 */
export default function Button (props) {
    var clickFunction = props.function
    const userData = props.userData;
    if(props.what == 'change'){
        clickFunction = () => {
            editUser(userData);
        }
    }else if(props.what == 'delete'){
        clickFunction = () => {
            deleteUser(userData);
        }
    }

    return (
        <div id={props.divType}>
            <button type="button" id={props.special} onClick={clickFunction}>{props.text}</button>
        </div>
    )
}

