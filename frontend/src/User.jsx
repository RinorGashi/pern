// imports
import React from 'react';
import Button from "./Button";
import './User.scss';

/**
    This is a User-Card. It is a visual representation of a User
    @Param props - properties
    @Return JSX-Element
 */
export default function User (props) {
    // This initializes a userData object
    const userData = {
        "firstName": props.fn,
        "lastName": props.ln.toString(),
        "gender": props.g,
        "password": props.pw
    }
    return (
        <div className="User" onClick={props.func}>
            <h1>{props.fn} {props.ln}</h1>
            <p>Gender</p>
            <p className="attribute">{props.g}</p>
            <p>Password</p>
            <p className="attribute">{props.pw}</p>
            <span id="EDITnDELETE">
                <Button text="edit" special="user" divType="userButtonDiv"  what="change" userData={userData}></Button>
                <Button text="delete" special="user" divType="userButtonDiv" what="delete" userData={userData}></Button>
            </span>
        </div>
    )
}