// imports
import React from 'react';
import './AddUser.scss';
import Button from "./Button";
import submit from "./common/Submit";

// variables
var currGender = "None";

/**
 This is the AddUser-Subpage
 @Param props - properties
 @Return JSX-Element
 */
export default function AddUser(props) {
    return (
        <div className="main">
            <div className="Parameter" id="firstName">
                <p>First Name</p>
                <input id="fNI"/>
            </div>
            <div className="Parameter" id="lastName">
                <p>Last Name</p>
                <input id="lNI"/>
            </div>
            <div className="Parameter" id="gender">
                <p>GENDER</p>
                <Button text="MALE" special="gender" function={() => choseGender("male")}/>
                <Button text="FEMALE" special="gender" function={() => choseGender("female")}/>
                <Button text="ATTACKHELICOPTER" special="gender"
                        function={() => choseGender("attackhelicopter")}/>
            </div>
            <div className="Parameter" id="password">
                <p>Password</p>
                <input type="password" id="pwOne"/>
            </div>
            <div className="Parameter" id="repPassword">
                <p>Repeat Password</p>
                <input type="password" id="pwTwo"/>
            </div>
            <div id="pwNotification">

            </div>
            <div className="Parameter">
                <Button text="SUBMIT" special="submit" function={() => submit(currGender)}/>
            </div>
        </div>
    );
}

/**
 This function updates the variable 'currGender' with the chose gender. It is invoked when clicked on a gender-button
 @Info this method will be removed and added to a seperate file - because of redundancy
 @Param The newly chosen gender
 @Return no return
 */
const choseGender = (gender) => {
    currGender = gender;
}